# Totem to csv

Outil d'extraction des données contenues dans les fichiers XML-TOTEM.

## Fonctionnalités

**Extraction des données budgétaires** : 

* extraction des données budgétaires conformément au schéma Budget SCDL 
* fonctionne pour l'ensemble des normes comptables des différents types de collectivités locales ou établissements publics
* fonctionne pour les différents étapes de l'exercice budgétaire (Budget prévisionnel, budget modificatif, compte administratif)

**Extraction des données de subvention**

* extraction des données conformément au schéma Subventions SCDL

**Extraction des données de patrimoine**

**Extraction des données de relative à la gestion de la dette**

**Extraction des données relatives aux dépenses de personnel**

## Contenu et installation

Ce dépôt de code contient actuellement : 

* les schémas XSD du schéma TOTEM
* les plans de compte de la compabilité publique relative aux collectivités locales et établissements publics
* un script python qui transforme le fichier xml fourni en ligne de commande en plusieurs fichiers csv et les copie dans un classeur numérique au format .ods (LibreOffice) 

## Dépendances logicielles

* Python version 3.*
* xmlstarlet
* bash

Les instructions d'installation et d'utilisation du script en ligne de commande sont précisées dans le fichier [README](https://gitlab.com/datafin/totem/-/tree/master/totem2csv/README.md) du dossier